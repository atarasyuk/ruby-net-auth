class AccessController < ApplicationController
  def token_state
    @access      = Access.new(access_params)
    @token_state = @access.get_token_state
    @error       = @token_state[:error] if @token_state.key?(:error)

    respond_to do |f|
      f.js { render @error.blank? ? 'access/token_state' : 'access/error' }
    end
  end

  def access_params
    params
      .require(:access)
      .permit(
        :access_token,
        :expires_in,
        :token_type
      )
  end
end