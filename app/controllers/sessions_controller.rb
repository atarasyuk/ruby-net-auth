class SessionsController < ApplicationController
  def index
    @user = User.new
  end

  def sign_in
    @user   = User.new(user_params).sign_in
    @error  = @user[:error] if @user.key?(:error)
    @access = Access.new(@user) if @error.blank?

    respond_to do |f|
      f.js { render @error.blank? ? 'access/index' : 'error' }
    end
  end

  private

  def user_params
    params
      .require(:user)
      .permit(
        :client_id,
        :grant_type,
        :username,
        :password,
        :client_secret,
        :scope
      )
  end
end
