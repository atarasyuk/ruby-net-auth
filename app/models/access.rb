class Access
  include ActiveModel::Model
  include Auth

  attr_accessor :access_token,
    :expires_in,
    :token_type

  def get_token_state
    connect_introspect(access_token)
  end
end