class User
  include ActiveModel::Model
  include Auth

  attr_accessor :client_id,
    :grant_type,
    :username,
    :password,
    :client_secret,
    :scope

  def sign_in
    connect_token(self.as_json)
  end
end