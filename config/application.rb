require_relative 'boot'

require "rails"
require "active_model/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "sprockets/railtie"
require "rails/test_unit/railtie"

Bundler.require(*Rails.groups)

module AuthTest
  class Application < Rails::Application
    config.autoload_paths << Rails.root.join('lib')
  end
end
