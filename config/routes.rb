Rails.application.routes.draw do
  match 'access_token_state', to: 'access#token_state', via: [:post]
  match 'session_sign_in',    to: 'sessions#sign_in',   via: [:post]

  root to: 'sessions#index'
end
