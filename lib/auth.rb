require 'json'
require 'uri'

module Auth
  CONFIG = Rails.application.config_for(:auth)

  def connect_token(params)
    begin
      res = request
        .headers(Accept: 'application/json')
        .post("#{ CONFIG['auth_server'] }/connect/token", body: URI.encode_www_form(params))

      parse(res)
    rescue Exception => e
      { error: e.message }
    end
  end

  def connect_introspect(token)
    begin
      res = request
        .headers(Authorization: "Basic #{ CONFIG['secret'] }")
        .post("#{ CONFIG['auth_server'] }/connect/introspect", body: URI.encode_www_form(token: token))

      parse(res)
    rescue Exception => e
      { error: e.message }
    end
  end

  private
  
  def parse(res)
    Hash[JSON.parse(res.body).map { |(k, v)| [k.to_sym, v] }]
  end

  def request
    HTTP
      .timeout(write: 5, connect: 5, read: 5)
      .headers('Content-Type': 'application/x-www-form-urlencoded')
  end
end
